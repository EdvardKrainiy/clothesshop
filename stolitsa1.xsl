<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="shop">
        <tr>
            <td><b><xsl:value-of select="../@type"/></b></td>
            <td><a href="//amazon.com/{@name}"> <xsl:value-of select="@name"/>.com</a></td>
            <td><b>Floor, where this shop places: </b><xsl:value-of select="@floor"/></td>
            <td><b>Rating of the shop: </b><xsl:value-of select="@range"/></td>
        </tr>
    </xsl:template>
    <xsl:template match="video">
        <tr>
            <td><b>Author of video: </b><xsl:value-of select="@author"/></td>
            <td><xsl:value-of select="@type"/></td>
            <td>Length of the video: <xsl:value-of select="@length"/></td>
        </tr>
    </xsl:template>

</xsl:stylesheet>