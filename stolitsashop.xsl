<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <body>
                <h1>The best clothing shops for men and women</h1>
                <div>
                    <p> <h2>Shops amount:</h2> <xsl:value-of select="count(//shop)"/></p>
                    <p><h2>Videos, which length is less than 200:</h2> <xsl:value-of select="//video[@length &lt;=200]/@type"/></p>
                </div>
                <table>
                    <xsl:apply-templates select="//video"/>
                </table>
                <h2>The best shops, chosen by professionals:</h2>
                <table>
                    <xsl:apply-templates select="//shop">
                        <xsl:sort select="@range" order="descending" data-type="number"/>
                    </xsl:apply-templates>
                </table>
            </body>
        </html>
    </xsl:template>
    <xsl:import href="stolitsa1.xsl"/>
</xsl:stylesheet>